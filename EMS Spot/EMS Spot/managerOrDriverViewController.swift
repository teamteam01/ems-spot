//
//  managerOrDriverViewController.swift
//  EMS Spot
//
//  Created by Biradar, Pravin on 7/10/17.
//  Copyright © 2017 Quicken Loans. All rights reserved.
//

import UIKit

class managerOrDriverViewController: UIViewController {

    @IBAction func itsAdmin(_ sender: Any) {
        let managerVC = self.storyboard?.instantiateViewController(withIdentifier: "ManagerViewController")
        self.navigationController?.pushViewController(managerVC!, animated: true)

    }
    @IBAction func itsDriver(_ sender: Any) {
        let driverVC = self.storyboard?.instantiateViewController(withIdentifier: "FetchEventsTableViewController")
        self.navigationController?.pushViewController(driverVC!, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
