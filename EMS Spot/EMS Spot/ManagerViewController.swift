//
//  MapViewController.swift
//  EMS Spot
//
//  Created by Pravin Biradar on 7/2/17.
//  Copyright © 2017 Quicken Loans. All rights reserved.
//

import Foundation
import UIKit

class ManagerViewController:UIViewController{

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func eventRegisterButton(_ sender: Any) {
        // To navigate to the event registration form
        let eventRegisterVC = self.storyboard?.instantiateViewController(withIdentifier: "eventRegisterVC")
        self.navigationController?.pushViewController(eventRegisterVC!, animated: true)    }
    
    @IBAction func upcomingEventHistoryButton(_ sender: Any) {
    }
    @IBAction func eventHistoryButton(_ sender: Any) {
    }
    @IBAction func uploadRouteButton(_ sender: Any) {
    }
}
