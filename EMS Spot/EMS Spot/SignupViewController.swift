//
//  SignupViewController.swift
//  EMS Spot
//
//  Created by Pravin Biradar on 7/2/17.
//  Copyright © 2017 Quicken Loans. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase

class SignupViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    
    @IBAction func roleSegment(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0
        {
            self.role = "Admin"
        }
        else if sender.selectedSegmentIndex == 1{
            self.role = "Driver"
        }
    }
    
    var role = String()
    
////    @IBAction func createAccount(_ sender: Any) {
////        if let username = usernameText.text , let password = passwordText.text{
////            Auth.auth().createUser(withEmail: username, password: password, completion: { (user, err) in
////                if let error = err
////                {
////                    print(error.localizedDescription)
////                }
////                else{
////                    print("Signe-Up Success")
////                }
////            })
////        }
////
////        
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    
    private func registerUserIntoDatabase(_ uid: String, values: [String: AnyObject]) {
        // Adding User Info
        let ref = Database.database().reference()
        let usersReference = ref.child("users").child(uid)
        
        usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
            if err != nil {
                print(err ?? "Error")
                return
            }
            print("Successfully Added a New User to the Database")
        })
    }
    
    

    
    
}
