//
//  FetchEventsTableViewController.swift
//  EMS Spot
//
//  Created by Biradar, Pravin on 7/12/17.
//  Copyright © 2017 Quicken Loans. All rights reserved.
//

import UIKit
import Firebase

class FetchEventsTableViewController: UITableViewController, UISearchResultsUpdating {

    var events = [Event]()
    
    var filteredArray = [Event]()
    var searchController = UISearchController(searchResultsController: nil)

    var on = false

    override func viewDidLoad() {
        super.viewDidLoad()

        fetchEvent()
        
        self.searchController.loadViewIfNeeded()
        searchController.searchResultsUpdater = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchTapped))

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        searchController.searchBar.removeFromSuperview()
    }
    func searchTapped(){
        
        if on == false
        {
            on = true
            
            navigationItem.titleView = searchController.searchBar
            searchController.searchBar.sizeToFit()
        }
        else{
            on = false
            navigationItem.titleView = nil
            navigationItem.title = String("Events List")
            
        }
    }
    
    func fetchEvent(){
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let current = dateFormatter.string(from: currentDate)
        
        
        Database.database().reference().child("events").queryOrdered(byChild: "date").queryStarting(atValue: current).observe(.childAdded, with: { (snapshot) in
            if let dict = snapshot.value as? [String:AnyObject]{
                let event = Event()
                event.setValuesForKeys(dict)
                self.events.append(event)
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive  && searchController.searchBar.text != "" {
            return self.filteredArray.count
        }
        else{
        return events.count
        }
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if searchController.isActive  && searchController.searchBar.text != "" {
            let event = filteredArray[indexPath.row]
            
            let name = tableView.viewWithTag(1) as! UILabel
            let dest = tableView.viewWithTag(2) as! UILabel
            let date = tableView.viewWithTag(3) as! UILabel
            
            name.text = event.name
            dest.text = event.destination
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let xyzDate = dateFormatter.date(from: event.date!)
            dateFormatter.dateFormat = "EEEE, MMM dd, yyyy' | 'h:mm a"
            
            let actualDate = dateFormatter.string(from: xyzDate!)
            date.text = actualDate
        }
        else{
            
            let event = events[indexPath.row]
            
            let name = tableView.viewWithTag(1) as! UILabel
            let dest = tableView.viewWithTag(2) as! UILabel
            let date = tableView.viewWithTag(3) as! UILabel
            
            name.text = event.name
            dest.text = event.destination
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let xyzDate = dateFormatter.date(from: event.date!)
            dateFormatter.dateFormat = "EEEE, MMM dd, yyyy' | 'h:mm a"
            
            let actualDate = dateFormatter.string(from: xyzDate!)
            date.text = actualDate
        }
        


        return cell
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //hideSearchBar()
        navigationItem.titleView = nil
        searchBar.isHidden = true
    }
    
    func filterContent(searchString:String){
        filteredArray = events.filter(){ nil != $0.destination?.localizedLowercase.range(of: searchString.localizedLowercase)}
        tableView.reloadData()
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        self.filterContent(searchString: searchController.searchBar.text!  )
    }

   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "eventDetails" ,
            let nextScene = segue.destination as? DriverViewController ,
            let indexPath = self.tableView.indexPathForSelectedRow {
            if searchController.isActive  && searchController.searchBar.text != "" {
                let selectedEvent = filteredArray[indexPath.row]
                nextScene.eventFromFirebase = selectedEvent
                nextScene.theGenerator = "tableView"
            }
            else{
                let selectedEvent = events[indexPath.row]
                nextScene.eventFromFirebase = selectedEvent
                nextScene.theGenerator = "tableView"
            }
        }
    }

}
