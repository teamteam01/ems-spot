//
//  Event.swift
//  EMS Spot
//
//  Created by Biradar, Pravin on 7/12/17.
//  Copyright © 2017 Quicken Loans. All rights reserved.
//

import UIKit

class Event: NSObject {
    var number:String?
    var name: String?
    var eventVenue:String?
    var coordinator: String?
    var coordinatorNumber:String?
    var shuttleCompany:String?
    var date: String?
    var source: String?
    var eventEndTime:String?
    var destination: String?
    var numOfShuttles: String?
    var expectedCount: String?
    var lat : String?
    var long:String?
    var additionalText: String?
    var imageURLs:[String]?
}
