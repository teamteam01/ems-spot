//
//  ViewController.swift
//  EMS Spot
//
//  Created by Rohan Thorat on 6/30/17.
//  Copyright © 2017 Quicken Loans. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    @IBOutlet weak var loginOutlet: UIButton!
    
    @IBOutlet weak var dontHaveText: UILabel!
    
    @IBOutlet weak var createNewAccountOutlet: UIButton!
    
    var ref = DatabaseReference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        usernameText.delegate = self
//        passwordText.delegate = self
        eventNumberText.delegate = self
        
        ref = Database.database().reference(fromURL: "https://ems-transportation.firebaseio.com/")
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    

    @IBAction func itsDriverAction(_ sender: Any) {
        let driverVC = self.storyboard?.instantiateViewController(withIdentifier: "FetchEventsTableViewController")
        self.navigationController?.pushViewController(driverVC!, animated: true)
    }
    
    
    @IBAction func loginBtn(_ sender: Any) {
        if let username = usernameText.text , let password = passwordText.text{
            Auth.auth().signIn(withEmail: username, password: password, completion: { (user, err) in
                if let error = err{
                    print(error.localizedDescription)
                    
                    let alert = UIAlertController.init(title: "Alert", message: "\(error.localizedDescription)", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    print("User Logged In")
                    let managerVC = self.storyboard?.instantiateViewController(withIdentifier: "ManagerViewController")
                    self.navigationController?.pushViewController(managerVC!, animated: true)
                }
            })
        }
        
    }
    
    @IBAction func createAccount(_ sender: Any) {
        if let username = usernameText.text , let password = passwordText.text{
            Auth.auth().createUser(withEmail: username, password: password, completion: { (user, err) in
                if let error = err
                {
                    print(error.localizedDescription)
                    let alert = UIAlertController.init(title: "Alert", message: "\(error.localizedDescription)", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else{
                    print("Signe-Up Success")
                    let alert = UIAlertController.init(title: "Success", message: "User Signed Up Successfully", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }
    }
       
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
    }
    
    func hideKeyboard()
    {
        view.endEditing(true)
    }
    @IBOutlet weak var adminOutlet: UIButton!
    
    @IBOutlet weak var adminLabel: UILabel!
    @IBOutlet weak var driverOutlet: UIButton!
    @IBOutlet weak var driverLabel: UILabel!
    
    @IBOutlet weak var eventDetailsOutlet: UIButton!
    
    @IBOutlet weak var eventNumberText: UITextField!
    
    @IBAction func adminAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: { 
            self.adminOutlet.center = CGPoint(x: self.passwordText.center.x - self.passwordText.frame.width/2 - self.adminOutlet.frame.width/2, y: self.adminOutlet.center.y)
            
            self.adminLabel.center = CGPoint(x: self.passwordText.center.x - self.passwordText.frame.width/2 - self.adminLabel.frame.width/2, y: self.adminLabel.center.y)
            
            self.driverOutlet.center = CGPoint(x: self.passwordText.center.x + self.passwordText.frame.width/2 + self.driverOutlet.frame.width/2, y: self.driverOutlet.center.y)
            
            self.driverLabel.center = CGPoint(x: self.passwordText.center.x + self.passwordText.frame.width/2 + self.driverLabel.frame.width/2, y: self.driverLabel.center.y)
        }) { (_) in
            UIView.animate(withDuration: 0.4, animations: {
                
                self.driverLabel.isHidden = false
                self.eventNumberText.isHidden = true
                self.eventDetailsOutlet.isHidden = true
                self.usernameText.isHidden = false
                self.passwordText.isHidden = false
                self.loginOutlet.isHidden = false
                self.dontHaveText.isHidden = false
                self.createNewAccountOutlet.isHidden = false
                
            })
        }
        
    }
    
    @IBAction func driverAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: { 
            self.adminOutlet.center = CGPoint(x: self.passwordText.center.x - self.passwordText.frame.width/2 - self.adminOutlet.frame.width/2, y: self.adminOutlet.center.y)
            
            
            self.adminLabel.center = CGPoint(x: self.passwordText.center.x - self.passwordText.frame.width/2 - self.adminLabel.frame.width/2, y: self.adminLabel.center.y)
            
            self.driverOutlet.center = CGPoint(x: self.passwordText.center.x + self.passwordText.frame.width/2 + self.driverOutlet.frame.width/2, y: self.driverOutlet.center.y)
            
            self.driverLabel.center = CGPoint(x: self.passwordText.center.x + self.passwordText.frame.width/2 + self.driverLabel.frame.width/2, y: self.driverLabel.center.y)
        }) { (_) in
            UIView.animate(withDuration: 0.4, animations: {
                self.driverOutlet.isHidden = false
                self.driverLabel.isHidden = false
                self.eventDetailsOutlet.isHidden = false
                self.eventNumberText.isHidden = false
                self.usernameText.isHidden = true
                self.passwordText.isHidden = true
                self.loginOutlet.isHidden = true
                self.dontHaveText.isHidden = true
                self.createNewAccountOutlet.isHidden = true
                
            })
        }
        
        
    }
    @IBAction func eventDetailsAction(_ sender: Any) {
        
        if eventNumberText.text == ""{
            let alert = UIAlertController.init(title: "Alert", message: "Event Number cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)

        }
        else{
            self.ref.child("events").observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.hasChild("\(self.eventNumberText.text!)"){
                    print("Event is present in the Database")
                    self.performSegue(withIdentifier: "mainToDriver", sender: self)
                }
                else{
                    let alert = UIAlertController.init(title: "Alert", message: "Event Number does not exists", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)

                }
            }, withCancel: nil)
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mainToDriver"{
        let nextScene = segue.destination as! DriverViewController
        nextScene.eventNumberFromUser = self.eventNumberText.text!
        nextScene.theGenerator = "mainView"
        }
        
    }
    
}

