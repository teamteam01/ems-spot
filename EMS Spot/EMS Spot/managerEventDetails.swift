//
//  managerEventDetails.swift
//  EMS Spot
//
//  Created by Rohan Thorat on 7/23/17.
//  Copyright © 2017 Quicken Loans. All rights reserved.
//

import UIKit
import GooglePlaces
import FirebaseDatabase
import LRTextField


class managerEventDetails: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    var event = Event()
    
    //Creating a firebase database reference
    var ref:DatabaseReference?
    
    var sod = String()
    var lat = String()
    var long = String()
    
    var historyOrUpcoming = String()
    
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var editSwitch: UISwitch!
    @IBOutlet weak var eventNumber: LRTextField!
    
    @IBOutlet weak var eventName: LRTextField!
    
    @IBOutlet weak var eventVenue: LRTextField!
    
    @IBOutlet weak var eventCoordinator: LRTextField!
    
    @IBOutlet weak var contactPhone: LRTextField!
    
    @IBOutlet weak var shuttleCompany: LRTextField!
    
    @IBOutlet weak var eventDate: UIDatePicker!
    
    @IBOutlet weak var source: LRTextField!
    
    @IBOutlet weak var eventEndTime: UIDatePicker!
    
    @IBOutlet weak var destination: LRTextField!
    
    @IBOutlet weak var numOfShuttles: LRTextField!
    
    @IBOutlet weak var expectedCount: LRTextField!
    
    @IBOutlet weak var additionalText: UITextView!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBAction func editSwitch(_ sender: Any) {
        
        if editSwitch.isOn{
            editModeOnAction()
        }
        else{
            editModeOffAction()
        }
    }
    
    @IBAction func submitAction(_ sender: Any) {
        
        self.validateFields()
        
    }
    
    func submitEventToFirebase(){
        let eventNumberUI = eventNumber.text!
        let eventNameUI = eventName.text!
        let eventVenueUI = eventVenue.text!
        let eventCoordinatorUI = eventCoordinator.text!
        let coordinatorNumber = contactPhone.text!
        let shuttleCompanyUI = shuttleCompany.text!
        
        //structuring the date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let eventDateUI = dateFormatter.string(from: eventDate.date)
        
        let sourceUI = source.text!
        let eventEndTimeUI = dateFormatter.string(from: eventEndTime.date)
        let destUI = destination.text!
        let numOfShuttlesUI = numOfShuttles.text!
        let expectedCountUI = expectedCount.text!
        let additionalTextUI = additionalText.text!
        
        
        
        ref = Database.database().reference(fromURL: "https://ems-transportation.firebaseio.com/")
        
        let eventRef = ref?.child("events").child("\(eventNumberUI)")
        
        let events = ["number": eventNumberUI, "name": eventNameUI,"eventVenue": eventVenueUI , "coordinator": eventCoordinatorUI ,"coordinatorNumber": coordinatorNumber ,"shuttleCompany":shuttleCompanyUI ,"date": eventDateUI,   "source": sourceUI,  "eventEndTime": eventEndTimeUI,"destination": destUI, "numOfShuttles": numOfShuttlesUI, "expectedCount": expectedCountUI,  "lat": lat, "long": long, "additionalText": additionalTextUI] as [String : Any]
        
        eventRef?.updateChildValues(events as Any as! [AnyHashable : Any], withCompletionBlock: { (error, ref ) in
            if error != nil {
                print(error!)
            }
            let alert = UIAlertController.init(title: "Alert", message: "Event Updated Successfully", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
                //                self.navigationController?.popViewController(animated: true)
                self.editModeOffAction()
            })
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
        })

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        //to hide the keyboard
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)

        lat = event.lat!
        long = event.long!
        editSwitch.isOn = false
        editModeOffAction()
        
        source.addTarget(self, action: #selector(selectSource(textField:)), for: UIControlEvents.editingDidBegin)
        destination.addTarget(self, action: #selector(selectDest(textField:)), for: UIControlEvents.editingDidBegin)
        additionalText.delegate = self
        eventNumber.delegate = self
        contactPhone.delegate = self
        numOfShuttles.delegate = self
        expectedCount.delegate = self
        
        let todaysDate = Date()
        eventDate.minimumDate = todaysDate
        eventEndTime.minimumDate = todaysDate
        
        additionalText.layer.borderColor = UIColor.gray.cgColor
        additionalText.layer.borderWidth = 1.0;
        additionalText.layer.cornerRadius = 5.0;
        
        setUpValues()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
    }
    
    //to hide the keyboard
    func hideKeyboard()
    {
        view.endEditing(true)
    }

    func setUpValues() {
        eventNumber.enableAnimation = false
        eventNumber.text = event.number
//        eventNumber.enableAnimation = true

        
        eventName.enableAnimation = false
        eventName.text = event.name
        eventName.enableAnimation = true

        
        eventVenue.enableAnimation = false
        eventVenue.text = event.eventVenue
        eventVenue.enableAnimation = true

        eventCoordinator.enableAnimation = false
        eventCoordinator.text = event.coordinator
        eventCoordinator.enableAnimation = true

        contactPhone.enableAnimation = false
        contactPhone.text = event.coordinatorNumber
        contactPhone.enableAnimation = true

        shuttleCompany.enableAnimation = false
        shuttleCompany.text = event.shuttleCompany
        shuttleCompany.enableAnimation = true

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        
        let actualEventDate = dateFormatter.date(from: event.date!)
        eventDate.date = actualEventDate!
        
        source.enableAnimation = false
        source.text = event.source
        source.enableAnimation = true


        let actualEventEndTime = dateFormatter.date(from: event.eventEndTime!)
        eventEndTime.date = actualEventEndTime!
        
        destination.enableAnimation = false
        destination.text = event.destination
        destination.enableAnimation = true

        numOfShuttles.enableAnimation = false
        numOfShuttles.text = event.numOfShuttles
        numOfShuttles.enableAnimation = true

        expectedCount.enableAnimation = false
        expectedCount.text = event.expectedCount
        expectedCount.enableAnimation = true

        additionalText.text = event.additionalText
        submitBtn.isHidden = true

    }
    
    func editModeOffAction(){
        
        eventNumber.isUserInteractionEnabled = false
        eventName.isUserInteractionEnabled = false
        eventVenue.isUserInteractionEnabled = false
        eventCoordinator.isUserInteractionEnabled = false
        contactPhone.isUserInteractionEnabled = false
        shuttleCompany.isUserInteractionEnabled = false
        eventDate.isUserInteractionEnabled = false
        source.isUserInteractionEnabled = false
        eventEndTime.isUserInteractionEnabled = false
        destination.isUserInteractionEnabled = false
        numOfShuttles.isUserInteractionEnabled = false
        expectedCount.isUserInteractionEnabled = false
        additionalText.isUserInteractionEnabled = false
        
        submitBtn.isHidden = true
        
        eventNumber.backgroundColor = UIColor.clear
        eventNumber.backgroundColor = UIColor.clear
        eventName.backgroundColor = UIColor.clear
        eventVenue.backgroundColor = UIColor.clear
        eventCoordinator.backgroundColor = UIColor.clear
        contactPhone.backgroundColor = UIColor.clear
        shuttleCompany.backgroundColor = UIColor.clear
        source.backgroundColor = UIColor.clear
        destination.backgroundColor = UIColor.clear
        numOfShuttles.backgroundColor = UIColor.clear
        expectedCount.backgroundColor = UIColor.clear
        additionalText.backgroundColor = UIColor.clear
    }
    
    func editModeOnAction(){
        if historyOrUpcoming == "history"{
            eventNumber.isUserInteractionEnabled = true
            eventNumber.backgroundColor = UIColor.white

        }else if (historyOrUpcoming == "upcoming"){
            eventNumber.isUserInteractionEnabled = false
            eventNumber.backgroundColor = UIColor.clear

        }
        
        eventName.isUserInteractionEnabled  = true
        eventVenue.isUserInteractionEnabled = true
        eventCoordinator.isUserInteractionEnabled = true
        contactPhone.isUserInteractionEnabled = true
        shuttleCompany.isUserInteractionEnabled = true
        eventDate.isUserInteractionEnabled = true
        source.isUserInteractionEnabled = true
        eventEndTime.isUserInteractionEnabled = true
        destination.isUserInteractionEnabled = true
        numOfShuttles.isUserInteractionEnabled = true
        expectedCount.isUserInteractionEnabled = true
        additionalText.isUserInteractionEnabled = true
        
        submitBtn.isHidden = false
        
        eventName.backgroundColor = UIColor.white
        eventVenue.backgroundColor = UIColor.white
        eventCoordinator.backgroundColor = UIColor.white
        contactPhone.backgroundColor = UIColor.white
        shuttleCompany.backgroundColor = UIColor.white
        source.backgroundColor = UIColor.white
        destination.backgroundColor = UIColor.white
        numOfShuttles.backgroundColor = UIColor.white
        expectedCount.backgroundColor = UIColor.white
        additionalText.backgroundColor = UIColor.white
    }
    
    func selectSource(textField: UITextField){
        sod = "Source"
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func selectDest(textField: UITextField){
        
        sod = "Dest"
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    func validateFields(){
        if eventNumber.text == ""
            
        {
            
            let alert = UIAlertController.init(title: "Alert", message: "Event Number cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if eventName.text == "" {
            
            let alert = UIAlertController.init(title: "Alert", message: "Event Title cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if eventVenue.text == "" {
            
            let alert = UIAlertController.init(title: "Alert", message: "Event Venue cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if eventCoordinator.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Event Team Contact Name cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if contactPhone.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Event Team Contact Phone cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if shuttleCompany.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Shuttle Company cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if source.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Pickup Location cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if destination.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Drop off Location cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if numOfShuttles.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Number of Shuttles cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if expectedCount.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Number of People cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
        else{
            submitEventToFirebase()
        }
        
    }
    

}
extension managerEventDetails: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place Coordinates:\(place.coordinate.latitude)")
        print("Place Longitude:\(place.coordinate.longitude) ")
        
        lat = String(place.coordinate.latitude)
        long = String(place.coordinate.longitude)
        
        dismiss(animated: true, completion: nil)
        
        if ( sod == "Source"){
            source.text = place.name
        }
            
        else if (sod == "Dest"){
            destination.text = place.name
        }
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

