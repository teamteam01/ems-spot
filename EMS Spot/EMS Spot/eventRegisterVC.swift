//
//  eventRegisterVC.swift
//  EMS Spot
//
//  Created by Rohan Thorat on 7/2/17.
//  Copyright © 2017 Quicken Loans. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import GoogleMaps
import GooglePlaces
import ImagePicker
import FirebaseStorage
import LRTextField

class eventRegisterVC: UIViewController, ImagePickerDelegate, UITextViewDelegate, UITextFieldDelegate {
    
    //Creating a firebase database reference
    var ref:DatabaseReference?
    
    var sod = String()
    var lat = String()
    var long = String()
    var imageURLs = [String]()
    
    let imagePickerController = ImagePickerController()
    var selectedImageArray = [UIImage]()
    
    @IBOutlet weak var eventNumber: LRTextField!
    
    @IBOutlet weak var eventName: UITextField!
    
    @IBOutlet weak var eventVenue: UITextField!
    
    @IBOutlet weak var eventCoordinator: UITextField!
    
    @IBOutlet weak var contactPhone: LRTextField!
    
    @IBOutlet weak var shuttleCompany: UITextField!
    
    @IBOutlet weak var eventDate: UIDatePicker!
    
    @IBOutlet weak var source: LRTextField!
    
    @IBOutlet weak var eventEndTime: UIDatePicker!
    
    @IBOutlet weak var destination: LRTextField!
    
    @IBOutlet weak var numOfShuttles: UITextField!
    
    @IBOutlet weak var expectedCount: UITextField!
    
    @IBOutlet weak var additionalText: UITextView!
    
    let alert = UIAlertController.init(title: "Alert", message: "Alert Message", preferredStyle: UIAlertControllerStyle.alert)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //to hide the keyboard
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        source.addTarget(self, action: #selector(selectSource(textField:)), for: UIControlEvents.editingDidBegin)
        destination.addTarget(self, action: #selector(selectDest(textField:)), for: UIControlEvents.editingDidBegin)
        additionalText.delegate = self
        eventNumber.delegate = self
        contactPhone.delegate = self
        numOfShuttles.delegate = self
        expectedCount.delegate = self
        
        let todaysDate = Date()
        eventDate.minimumDate = todaysDate
        eventEndTime.minimumDate = todaysDate
        
        additionalText.layer.borderColor = UIColor.gray.cgColor
        additionalText.layer.borderWidth = 1.0;
        additionalText.layer.cornerRadius = 5.0;
        
        contactPhone.style = .phone
        contactPhone.format = "###-###-####"
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        additionalText.text = ""
        return true
    }
    
    @IBAction func uploadImages(_ sender: Any) {
        
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func registerEvent(_ sender: Any) {
        validateFields()
        
    }
    
    func submitEventToFirebase(){
        let eventNumberUI = eventNumber.text!
        let eventNameUI = eventName.text!
        let eventVenueUI = eventVenue.text!
        let eventCoordinatorUI = eventCoordinator.text!
        let coordinatorNumber = contactPhone.text!
        let shuttleCompanyUI = shuttleCompany.text!
        
        //structuring the date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let eventDateUI = dateFormatter.string(from: eventDate.date)
        
        let sourceUI = source.text!
        let eventEndTimeUI = dateFormatter.string(from: eventEndTime.date)
        let destUI = destination.text!
        let numOfShuttlesUI = numOfShuttles.text!
        let expectedCountUI = expectedCount.text!
        let additionalTextUI = additionalText.text!

        ref = Database.database().reference(fromURL: "https://ems-transportation.firebaseio.com/")
        
        let eventRef = ref?.child("events").child("\(eventNumberUI)")
        
        let events = ["number": eventNumberUI, "name": eventNameUI,"eventVenue": eventVenueUI , "coordinator": eventCoordinatorUI ,"coordinatorNumber": coordinatorNumber ,"shuttleCompany":shuttleCompanyUI ,"date": eventDateUI,   "source": sourceUI,  "eventEndTime": eventEndTimeUI,"destination": destUI, "numOfShuttles": numOfShuttlesUI, "expectedCount": expectedCountUI,  "lat": lat, "long": long, "additionalText": additionalTextUI, "imageURLs": imageURLs] as [String : Any]
        
        eventRef?.updateChildValues(events as Any as! [AnyHashable : Any], withCompletionBlock: { (error, ref ) in
            if error != nil {
                print(error!)
            }
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
            
            self.alert.addAction(okAction)
            self.alert.title = "Success"
            self.alert.message = "\nNew Event Registered "

        })
        
    }
    
    func uploadImagetoFirebase(){
        let eventNumberUI = eventNumber.text!
        
        for i in 0..<selectedImageArray.count{
            
            let storageRef = Storage.storage().reference().child("\(eventNumberUI)").child("newImage\(i).png")
            
            if let uploadData = UIImagePNGRepresentation(selectedImageArray[i]){
                storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                    if error != nil{
                        print(error!)
                        return
                    }
                    if let imagesUrl = metadata?.downloadURL()?.absoluteString
                    {
                        self.imageURLs.append(imagesUrl)
                    }
                    print("uploaded")
                    print(self.imageURLs.count)
                    print(self.selectedImageArray.count)
                    if self.imageURLs.count == self.selectedImageArray.count{
                        self.submitEventToFirebase()
                    }
                })
            }
        }
        
    }
    //to hide the keyboard
    func hideKeyboard()
    {
        view.endEditing(true)
    }
    
    func selectSource(textField: UITextField){
        sod = "Source"
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func selectDest(textField: UITextField){
        
        sod = "Dest"
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }

    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage])
    {
        
    }
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]){
        
        selectedImageArray = images
        imagePickerController.dismiss(animated: true, completion: nil)
    }
    func cancelButtonDidPress(_ imagePicker: ImagePickerController){
        
    }

    func validateFields(){
        if eventNumber.text == ""
            
        {
            
            let alert = UIAlertController.init(title: "Alert", message: "Event Number cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if eventName.text == "" {
            
            let alert = UIAlertController.init(title: "Alert", message: "Event Title cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if eventVenue.text == "" {
            
            let alert = UIAlertController.init(title: "Alert", message: "Event Venue cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if eventCoordinator.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Event Team Contact Name cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if contactPhone.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Event Team Contact Phone cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if shuttleCompany.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Shuttle Company cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if source.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Pickup Location cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if destination.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Drop off Location cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if numOfShuttles.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Number of Shuttles cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else if expectedCount.text == ""{
            
            let alert = UIAlertController.init(title: "Alert", message: "Number of People cannot be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        else if (selectedImageArray.count <= 0){
            let photoAlert = UIAlertController.init(title: "Alert", message: "No Images have been added, Do you still want to continue ?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
                self.alert.title = "Please Wait"
                self.alert.message = "\n\nCreating New Event"
                self.present(self.alert, animated: true, completion: nil)
                self.submitEventToFirebase()
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                
                photoAlert.dismiss(animated: true, completion: nil)
                
            })
            
            photoAlert.addAction(okAction)
            
            photoAlert.addAction(cancelAction)
            
            self.present(photoAlert, animated: true, completion:  nil)
        }
        else{
            alert.title = "Please Wait"
            alert.message = "\n\nCreating New Event"
            self.present(alert, animated: true, completion: nil)
            self.uploadImagetoFirebase()
        }
        
    }
    

}

extension eventRegisterVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place Coordinates:\(place.coordinate.latitude)")
        print("Place Longitude:\(place.coordinate.longitude) ")
        
        lat = String(place.coordinate.latitude)
        long = String(place.coordinate.longitude)
        
        dismiss(animated: true, completion: nil)
        
        if ( sod == "Source"){
            source.enableAnimation = false
            source.text = place.name
            source.enableAnimation = true
        }
            
        else if (sod == "Dest"){
            destination.enableAnimation = false
            destination.text = place.name
            destination.enableAnimation = true
        }
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
