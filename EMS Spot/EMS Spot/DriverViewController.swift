//
//  DriverViewController.swift
//  EMS Spot
//
//  Created by Biradar, Pravin on 7/10/17.
//  Copyright © 2017 Quicken Loans. All rights reserved.
//

import UIKit
import MapKit
import FirebaseStorage
import Firebase

var imageCache = NSCache<AnyObject, AnyObject>()


class DriverViewController: UIViewController {
    var eventFromFirebase = Event()
    var images = [UIImage]()
    var eventNumberFromUser = String()
    var ref = DatabaseReference()
    var theGenerator = String()
    
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventDropOffLocation: UILabel!
    @IBOutlet weak var eventTime: UILabel!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet var popUpView: UIView!
    @IBOutlet weak var popUpLabel: UITextView!
    
    @IBOutlet weak var popUpCancelBtn: UIButton!
    
    @IBAction func popUpCancel(_ sender: Any) {
        popUpView.removeFromSuperview()
    }
    
    @IBAction func adminNoteAction(_ sender: Any) {
        popUpLabel.text = eventFromFirebase.additionalText
        popUpCancelBtn.layer.cornerRadius = 50.0
        popUpCancelBtn.layer.borderColor = UIColor.white.cgColor
        popUpCancelBtn.layer.borderWidth = 1.0
        popUpView.center = CGPoint(x: view.frame.width/2, y: view.frame.height*0.78)
        
        view.addSubview(popUpView)
    }
    
    @IBAction func contactAdminPopUp(_ sender: Any) {
        popUpLabel.text = eventFromFirebase.coordinatorNumber
        popUpLabel.textAlignment = .center
        popUpCancelBtn.layer.cornerRadius = 50.0
        popUpCancelBtn.layer.borderColor = UIColor.white.cgColor
        popUpCancelBtn.layer.borderWidth = 1.0
        popUpView.center = CGPoint(x: view.frame.width/2, y: view.frame.height*0.78)
        
        view.addSubview(popUpView)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference(fromURL: "https://ems-transportation.firebaseio.com/")

        if theGenerator == "tableView"{
            eventNumberFromUser = eventFromFirebase.number!
        }
        
        self.fetchData()
        
    }
    
    func fetchData(){
        ref.child("events").child("\(eventNumberFromUser)").observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? [String:AnyObject]{
                
                self.eventFromFirebase.setValuesForKeys(dict)
                
                self.downloadImages()
                
                self.mainScrollView.frame = self.view.frame
                
                self.eventName.text = self.eventFromFirebase.name
                self.eventDropOffLocation.text = self.eventFromFirebase.destination
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
                let xyzDate = dateFormatter.date(from: self.eventFromFirebase.date!)
                dateFormatter.dateFormat = "EEEE, MMM dd, yyyy' | 'h:mm a"
                
                let actualDate = dateFormatter.string(from: xyzDate!)
                self.eventTime.text = actualDate
                
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hidePopUpView))
                self.view.addGestureRecognizer(tapGesture)
            }
        }, withCancel: nil)
    }
    
    func hidePopUpView(){
        popUpView.removeFromSuperview()
    }
    
    func downloadImages(){
        if let imageURLs = eventFromFirebase.imageURLs{
            for i in 0..<imageURLs.count{
                let url = URL(string: imageURLs[i])
                print(url!)
                
                downloadImage(url: url!)
            }
            
        }
        
    }

    @IBAction func showDirections(_ sender: Any) {
        let lat:CLLocationDegrees = Double(eventFromFirebase.lat!)!
        let long:CLLocationDegrees = Double(eventFromFirebase.long!)!
        
        
        let regionDistance:CLLocationDistance = 1000;
        let coordinates = CLLocationCoordinate2DMake(lat, long)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [MKLaunchOptionsMapCenterKey: NSValue (mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey:NSValue (mkCoordinateSpan: regionSpan.span)]
        
        let placemark = MKPlacemark(coordinate: coordinates)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = eventFromFirebase.destination
        mapItem.openInMaps(launchOptions: options)
        

    }

    func showImagestoView(){
        for i in 0..<images.count{
            let imageView = UIImageView()
            imageView.image = images[i]
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            let xPosition = self.view.frame.width * CGFloat(i)
            imageView.frame = CGRect(x: xPosition, y: 0, width: self.mainScrollView.frame.width, height: self.mainScrollView.frame.height)
            mainScrollView.contentSize.width = mainScrollView.frame.width * CGFloat(i+1)
            mainScrollView.addSubview(imageView)
            
        }
    
    }
    
    func downloadImage(url:URL){
        if let cachedImage = imageCache.object(forKey: url as AnyObject) as? UIImage{
            self.images.append(cachedImage)
            self.showImagestoView()
            print("chached Image")
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            print(response?.suggestedFilename ?? url.lastPathComponent)
            DispatchQueue.main.async() { () -> Void in
                
                if let downloadedImage = UIImage(data: data!){
                    imageCache.setObject(downloadedImage, forKey: url as AnyObject)
                    self.images.append(UIImage(data: data!)!)
                    self.showImagestoView()
                    print("downloaded Image")
                    print(self.images)
                }
                
                }
            }.resume()
    }
    
    
}


